import React, { Component } from 'react';
import { Card, Button } from 'semantic-ui-react';
import factory from '../ethereum/factory';
import Layout from '../components/layout';
import { Link } from '../routes';


class CampaignIndex extends Component {
  //no need for componentDidMount() when using Next
  static async getInitialProps() {                                              //static keyword denotes class function, not instance function.
    const campaigns = await factory.methods.getDeployedCampaigns().call();      //need to load components from Next server. calls the function without needing to render component too

    return { campaigns };
  }

  renderCampaigns() {
    const items = this.props.campaigns.map(address => {
      return {
        header: address,
        description: (
          <Link route={`/campaigns/${address}`}>
            <a>View Campaign</a>
          </Link>
        ),
        fluid: true
      };
    });
    return <Card.Group items={items} />;
  }

  render() {
    return (
      <Layout>
      <div>
        <h3>Open Campaigns</h3>
        <Link route="/campaigns/new">
          <a>
            <Button
              floated="right"
              content="Create Campaign"
              icon="add circle"
              primary
             />
          </a>
         </Link>
        {this.renderCampaigns()}
      </div>
      </Layout>
    );
  };

}


export default CampaignIndex;
