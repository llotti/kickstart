import React, { Component } from 'react';
import { Card, Grid, Button } from 'semantic-ui-react';
import Layout from '../../components/layout';
import Campaign from '../../ethereum/campaign';
import web3 from 'web3';
import ContributeForm from '../../components/contributeForm';
import { Link } from '../../routes';

class CampaignShow extends Component {
  static async getInitialProps(props) {
    const campaign = Campaign(props.query.address); // actual address of the campaign

    const summary = await campaign.methods.getSummary().call();

    return {
      address: props.query.address,
      minContribution: summary[0],
      balance: summary[1],
      requestsCount: summary[2],
      approversCount: summary[3],
      manager: summary[4]
    };
  }

  renderCards() {
    const {
      balance,
      minContribution,
      requestsCount,
      approversCount,
      manager
    } = this.props;

    const items = [
    {
      header: manager,
      meta: 'Address of Manager',
      description: 'The manager created this campaign and can create requests and withdraw money',
      style: { overflowWrap: 'break-word'}
    },

    {
      header: minContribution,
      meta: 'Minimum Contribution (wei)',
      description: 'You must contribute at least this amount of wei'
    },

    {
      header: requestsCount,
      meta: 'Requests count',
      description: '# of open requests for this campaign'
    },

    {
      header: approversCount,
      meta: 'Approvers Count',
      description: '# of people who have already contributed to the contract'
    },

    {
      header: web3.utils.fromWei(balance, 'ether'),
      meta: 'Campaign balance (ether)',
      description: 'How much eth available for the campaign'
    }

    ];

    return <Card.Group items={items} />;
  }

  render() {
    return (
      <Layout>
        <h3>Campaign Show</h3>
        <Grid>
          <Grid.Row>
            <Grid.Column width={10}>
              {this.renderCards()}
            </Grid.Column>
            <Grid.Column width={6}>
               <ContributeForm address={this.props.address} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Link route={`/campaigns/${this.props.address}/requests`}>
                <a>
                  <Button primary>View Requests</Button>
                </a>
              </Link>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Layout>
    );
  }
};

export default CampaignShow;
