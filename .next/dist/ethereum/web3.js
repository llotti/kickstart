'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var web3 = void 0;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  // check if web3 is running in the browser (i.e. if it's not undefined)
  web3 = new _web2.default(window.web3.currentProvider);
} else {
  // we are on the server OR the user isn't running metamask
  var provider = new _web2.default.providers.HttpProvider( //make your own provider from web3
  'https://ropsten.infura.io/v3/0d2a4362c46b4bddaf8a054551c1269b' //infura node URL
  );
  web3 = new _web2.default(provider);
}

exports.default = web3;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV0aGVyZXVtL3dlYjMuanMiXSwibmFtZXMiOlsiV2ViMyIsIndlYjMiLCJ3aW5kb3ciLCJjdXJyZW50UHJvdmlkZXIiLCJwcm92aWRlciIsInByb3ZpZGVycyIsIkh0dHBQcm92aWRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsQUFBTzs7Ozs7O0FBRVAsSUFBSSxZQUFKOztBQUVBLElBQUksT0FBQSxBQUFPLFdBQVAsQUFBa0IsZUFBZSxPQUFPLE9BQVAsQUFBYyxTQUFuRCxBQUE0RCxhQUFhLEFBQ3ZFO0FBQ0E7U0FBTyxBQUFJLGtCQUFLLE9BQUEsQUFBTyxLQUF2QixBQUFPLEFBQXFCLEFBQzdCO0FBSEQsT0FHTyxBQUNMO0FBQ0E7TUFBTSxlQUFlLGNBQUEsQUFBSyxVQUFULEFBQW1CLGNBQThCLEFBQ2hFO0FBRGUsa0VBQWpCLEFBQWlCLEFBQ2lELEFBRWxFO0FBSGlCO1NBR1YsQUFBSSxrQkFBWCxBQUFPLEFBQVMsQUFFakI7QUFFRDs7a0JBQUEsQUFBZSIsImZpbGUiOiJ3ZWIzLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9sYXVyYWxvdHRpL2NvZGUvbGxvdHRpL2tpY2tzdGFydCJ9