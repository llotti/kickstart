pragma solidity ^0.4.17;

contract CampaignFactory {
    address[] public deployedCampaigns;

    function createCampaign(uint minimum) public {
        address newCampaign = new Campaign(minimum, msg.sender); // deploys new instance of campaign contract, msg.sender being the creator
        deployedCampaigns.push(newCampaign);
    }

    function getDeployedCampaigns() public view returns (address[]) {
        return deployedCampaigns;
    }
}

contract Campaign {
    struct Request {
        string description;
        uint value;
        address recipient;
        bool complete;
        uint approvalCount; // keeps track of YES votes
        mapping(address => bool) approvals; // addresses of people that have approved, i.e. voted yes (true), on spending request
    }

    Request[] public requests;
    address public manager;
    uint public minContribution;
    mapping(address => bool) public approvers; // people that have donated, so allowed to vote (set to true for each donating address in contribute function)
    uint public approversCount; // new state variable to count how many contributors to the request exist

    modifier restricted() {
        require(msg.sender == manager);
        _;
    }

    constructor(uint minimum, address creator) public {
        manager = creator;
        minContribution = minimum;
    }

    function contribute() public payable {
        require(msg.value > minContribution);

        approvers[msg.sender] = true;
        approversCount++;
    }

    function createRequest(string description, uint value, address recipient)
    public restricted {
        Request memory newRequest = Request({
           description: description,
           value: value,
           recipient: recipient,
           complete: false,
           approvalCount: 0
        });

        requests.push(newRequest);
    }

    function approveRequest(uint index) public { //index of request to approve
        Request storage request = requests[index];

        require(approvers[msg.sender]); //require that msg.sender is allowed to vote
        require(!request.approvals[msg.sender]); //require that msg.sender has NOT voted yet on that request

        request.approvals[msg.sender] = true; //once msg.sender approves request sets to true
        request.approvalCount++; // increase the count of yes votes
    }

    function finalizeRequest(uint index) public restricted {
        Request storage request = requests[index];

        require(request.approvalCount > (approversCount / 2));
        require(!request.complete);

        request.recipient.transfer(request.value);
        request.complete = true;
    }

    function getSummary() public view returns (uint, uint, uint, uint, address) {
      return (
        minContribution,
        this.balance,
        requests.length,
        approversCount,
        manager
      );
    }

    function getRequestsCount() public view returns (uint) {
      return requests.length;
    }

}
