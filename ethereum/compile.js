const path = require('path');
const fs = require('fs-extra');
const solc = require('solc');

//1. Delete build folder
const buildPath = path.resolve(__dirname, 'build');
fs.removeSync(buildPath);

//2. Read campaign.sol from contracts folder
const campaignPath = path.resolve(__dirname, 'contracts', 'campaign.sol');
const source = fs.readFileSync(campaignPath, 'utf8');

//3. Compile both contracts
const output = solc.compile(source, 1).contracts; //output contains 2 contracts

//4. Write compiled output in build folder (one file per contract)
fs.ensureDirSync(buildPath); //recreates build folder

for (let contract in output) {
  fs.outputJsonSync(
    path.resolve(buildPath, contract.replace(':', '') + '.json'),
    output[contract]
  );
}
