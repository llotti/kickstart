import Web3 from 'web3';

let web3;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  // check if web3 is running in the browser (i.e. if it's not undefined)
  web3 = new Web3(window.web3.currentProvider);
} else {
  // we are on the server OR the user isn't running metamask
  const provider = new Web3.providers.HttpProvider(                 //make your own provider from web3
    'https://ropsten.infura.io/v3/0d2a4362c46b4bddaf8a054551c1269b' //infura node URL
    );
  web3 = new Web3(provider);

}

export default web3;
