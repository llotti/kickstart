import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json';

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  '0x2b2950CF71B7A8bB4729E9086012FB04752A2397'
);

export default instance;
